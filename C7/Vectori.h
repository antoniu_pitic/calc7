#pragma once

void Citire(int[], int&);
void Afisare(int[], int);

void GenerarePrinRepetareaUneiValori(int[], int&, int);
void Generare2(int[], int&);
void Generare3(int[], int&);

void GenerarePrinRepetareaUneiValoriRecursiv(int[], int&, int);

void Generare2Recursiv(int[], int&);
void Generare3Recursiv(int[], int&);

void GenerareRandom(int[], int&);
void Fibo(int[], int&);

bool VerificaCrescator(int[], int);
bool VerificaFibo(int[], int);

void Linie();

void BubbleSort(int[], int);
void SelectSort(int[], int);
void InsertSort(int[], int);

void CountSort(int[], int, int);
void Interclasare(int[], int, int[], int, int[], int &);

