#include <iostream>
using namespace std;
#include "Vectori.h"
#include <time.h>

int main() {
	int a[200000], n;

	srand(time(NULL));

	n = 200000;
	GenerareRandom(a, n);
	
	int timpVechi = time(NULL);
	CountSort(a, n, 100);
	cout << time(NULL) - timpVechi;

}