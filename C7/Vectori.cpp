#include "Vectori.h"
#include <iostream>
using namespace std;

void Citire(int a[], int &n)
{
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> a[i];
	}

}

void Afisare(int a[], int n)
{
	for (int i = 0; i < n; i++) {
		cout << a[i] << " ";
	}
	cout << endl;
}

void GenerarePrinRepetareaUneiValori(int a[], int &n, int x)
{
	n = 5+rand()%10;
	for (int i = 0; i < n; i++) {
		a[i] = x;
	}
}

void Generare2(int a[], int &n)
{
	n = 5+rand()%10;
	for (int i = 0; i < n; i++) {
		a[i] = i+1;
	}

}

void Generare3(int x[], int &lX)
{
	lX = 5 + rand() % 10;
	for (int i = 0; i < lX; i++) {
		x[i] = (i%2)?i:0;
	}
}

void GenerarePrinRepetareaUneiValoriRecursiv(int a[], int &n, int x)
{
	n = 5 + rand() % 10;
	a[0] = x;
	for (int i = 1; i < n; i++) {
		a[i] = a[i - 1];
	}

}

void Generare2Recursiv(int a[], int &n)
{
	n = 5 + rand() % 10;
	a[0] = 1;
	for (int i = 1; i < n; i++) {
		a[i] = a[i - 1] + 1;
	}
}

void Generare3Recursiv(int a[], int &n)
{
	n = 5 + rand() % 10;
	a[0] = 0; a[1] = 1;
	for (int i = 2; i < n; i++) {
		if (i % 2 == 0) {
			a[i] = a[i - 2];
		}
		else {
			a[i] = a[i - 2] + 2;
		}
	}
}

void GenerareRandom(int a[], int &n)
{
	//n = 5 + rand() % 5;
	for (int i = 0; i < n; i++) {
		a[i] = rand()%100;
	}

}

void Fibo(int a[], int &n)
{
	n = 5 + rand() % 10;
	a[0] = a[1] = 1;
	for (int i = 2; i < n; i++) {
		a[i] = a[i-2]+a[i-1];
	}

}

bool VerificaCrescator(int a[], int n)
{
	for (int i = 0; i < n - 1; i++) {
		if (a[i] > a[i + 1]) {
			return false;
		}
	}
	return true;
}

bool VerificaFibo(int a[], int n)
{
	if (a[0] != 1 || a[1] != 1) {
		return false;
	}
	for (int i = 2; i < n - 1; i++) {
		if (a[i] != a[i- 1] + a[i- 2]) {
			return false;
		}
	}
	return true;
}

void Linie()
{
	cout << "-------------------------------------------\n";
}

void BubbleSort(int a[], int n)
{
	for (int t = 1; t <= n - 1; t++) {
		for (int i = 0; i < n - 1; i++) {
			if (a[i] > a[i + 1]) {
				swap(a[i], a[i + 1]);
			}
		}
	}
}

void CountSort(int a[], int n, int m)
{
	int ct[101];

	for (int i = 0; i <= m; i++) {
		ct[i] = 0;
	}

	for (int i = 0; i < n; i++) {
		ct[a[i]]++;
	}

	n = 0;
	for (int i = 0; i <= m; i++) {
		for (int t = 1; t <= ct[i]; t++) {
			a[n++] = i;
		}
	}
}

void Interclasare(	int a[], int n, 
					int b[], int m, 
					int c[], int &l)
{
	int i = 0, j = 0;
	l = 0;

	while (i < n && j < m) {
		if (a[i] < b[j]) {
			c[l++] = a[i++];
		}
		else {
			c[l++] = b[j++];
		}
	}

	while (i < n){
		c[l++] = a[i++];
	}
	while (j < m) {
		c[l++] = b[j++];
	}
}
